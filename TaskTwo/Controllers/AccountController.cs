﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TaskTwo.App_Start;
using TaskTwo.Models;
using static TaskTwo.App_Start.EmailService;
using Microsoft.Owin.Security.Google;

namespace TaskTwo.Controllers
{
    public class AccountController : Controller
    {

        private userManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<userManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
        }

        // GET: Customer
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> CreateUser(RegisterModel model)
        {
            IdentityResult result = null;

            if (ModelState.IsValid)
            {
                User user = new User
                {
                    UserName = model.Email,
                    Name = model.Name,
                    Surname = model.Surname,
                    DateRegister = DateTime.Now,
                    Email = model.Email
                };

                result = await UserManager.CreateAsync(user, model.Password);



                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: true, rememberBrowser: true);
                    

                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callback = Url.Action("ConfirmationAccount", "Account", new { userId = user.Id, code = code },
                        protocol: Request.Url.Scheme);

                    await UserManager.SendEmailAsync(user.Id, "Account confirmation",
                        "<a href=${callback}>Confirm</a> your account");

                    Response.Write("Confirm your account. Mail has sent to your email");

                    return Redirect("/");

                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User user = await UserManager.FindAsync(model.Email, model.Password);

                    if (user == null)
                    {
                        ModelState.AddModelError("", "Incorrect login or pass");
                    }
                    else
                    {
                        if (user.EmailConfirmed == true)
                        {
                            ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                            AuthenticationManager.SignOut();
                            AuthenticationManager.SignIn(new AuthenticationProperties
                            {
                                IsPersistent = true
                            }, claim);
                        }
                        
                        else
                        {
                            var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                            var callback = Url.Action("ConfirmationAccount", "Account", new { userId = user.Id, code = code },
                                protocol: Request.Url.Scheme);

                            await UserManager.SendEmailAsync(user.Id, "Account confirmation",
                                "<a href=${callback}>Confirm</a> your account");

                            Response.Write("Account has not confirmed. Mail has sent again");

                            return Redirect("/");
                        }

                        return RedirectToAction("Index", "Home");

                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GoogleLogin()
        {
            Session["Workaround"] = 0;
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("GoogleLoginCallback", new { returnUrl = "/Home/Index" })
            };
            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Google");
            return new HttpUnauthorizedResult();
        }

        [AllowAnonymous]
        public async Task<ActionResult> GoogleLoginCallback()
        {
            try
            {
                ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();

                if (loginInfo == null) throw new Exception("login is null");
                var isMatch = await UserManager.FindByEmailAsync(loginInfo.Email);



                if (isMatch == null)
                {

                    User newUser = new User
                    {
                        Email = loginInfo.Email,
                        UserName = loginInfo.Email,
                        Name = loginInfo.DefaultUserName,
                        Surname = loginInfo.DefaultUserName,
                        DateRegister = DateTime.Now
                    };

                    IdentityResult result = await UserManager.CreateAsync(newUser);

                    if (result.Succeeded)
                    {
                        result = await UserManager.AddLoginAsync(newUser.Id, loginInfo.Login);
                        if (!result.Succeeded)
                        {
                            ViewBag.Error = result.Errors;
                            return View("Error");
                        }
                    }
                    else
                    {
                        ViewBag.Error = result.Errors;
                        return View("Error");
                    }
                }

                User existingUser = await UserManager.FindByEmailAsync(loginInfo.Email);

                ClaimsIdentity claim = await UserManager.CreateIdentityAsync(existingUser, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);

                return Redirect("/Home/Index" ?? "/Login");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ConfirmationAccount()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }
    }
}