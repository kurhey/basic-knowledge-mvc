﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskTwo.Models;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace TaskTwo.Controllers
{
    public class CreatingController : Controller
    {
        private static readonly HttpClient client = new HttpClient();

        // GET: Creating
        [Authorize]
        public ActionResult Index()
        {
            System.Diagnostics.Debug.WriteLine(User.Identity.Name);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateProduct(Product product)
        {
            product.DateUpd = DateTime.Now;

            using (ApplicationContext db = new ApplicationContext())
            {
                if (ModelState.IsValid)
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    ViewBag.Status = "Added product";
                    return View("Index");
                }
                else
                {
                    ViewBag.Status = "Error";
                    return View("Index");
                }

            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateOrder(string id)
        {
            
            using (ApplicationContext db = new ApplicationContext())
            {
                List<Product> allProducts = db.Products.ToList();
                ViewData["userId"] = id;
                ViewData["products"] = allProducts;
                return View();
            }

        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateOrder()
        {

            string userId = User.Identity.GetUserId();

            string s = Request["checkProduct"];
            int[] prod_id = s.Split(',').Select(t => Convert.ToInt32(t)).ToArray();

            Order order = new Order() { UserId = userId, DatePurchase = DateTime.Now } ;
            

            
                try
                {
                    using (ApplicationContext db = new ApplicationContext())
                    {
                    order.User = db.Users.Where(t => t.Id == userId).First();

                        for (int i = 0; i < prod_id.Length; ++i)
                        {
                            var id = prod_id[i];
                            ProductOrder po = new ProductOrder();
                            po.ProductId = prod_id[i];
                            order.ProductOrders.Add(po);
                        }

                        db.Orders.Add(order);
                        db.SaveChanges();

                        

                        ViewBag.Status = "Order added";
                        
                    }
                return RedirectToAction("Index", "Details");
            }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                return View("Error");
            
        }
    }
}
