﻿using System;
using System.Web.Mvc;
using TaskTwo.Models;
using System.Xml.Serialization;
using System.IO;

namespace TaskTwo.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Serialization()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Serialization(Product product)
        {
            try
            {
                string fileName = Server.MapPath(".").ToString() + @"\..\product.xml";
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

                XmlSerializer formatter = new XmlSerializer(typeof(Product));

                using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, product);
                }

                ViewBag.Status = "Done";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }

        [Authorize]
        public ActionResult ShowXml()
        {
            try
            {
                Product product = new Product();

                string fileName = Server.MapPath(".").ToString() + @"\..\product.xml";
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

                XmlSerializer formatter = new XmlSerializer(typeof(Product));

                using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
                {
                    product = (Product)formatter.Deserialize(fs);
                }

                return View("ShowXmlPartial", product);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }
    }
}