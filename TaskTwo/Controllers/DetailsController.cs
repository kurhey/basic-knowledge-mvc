﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskTwo.Models;
using Microsoft.AspNet.Identity;

namespace TaskTwo.Controllers
{
    public class DetailsController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            try
            {
                string _id = User.Identity.GetUserId();
                using (ApplicationContext db = new ApplicationContext())
                {
                    System.Diagnostics.Debug.WriteLine(_id);
                    var user = db.Users.Where(t => t.Id == _id).First();
                    return View(user);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
            }
            return View("Error");
        }

        [Authorize]
        public ActionResult Products()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var products = db.Products.ToList();
                return View(products);
            }
        }

        [Authorize]
        public ActionResult Orders()
        {
            string userId = User.Identity.GetUserId();
            using (ApplicationContext db = new ApplicationContext())
            {
                var orders = db.Orders.Where(t => t.UserId == userId).ToList();

                List<OrderInfo> userInfoList = new List<OrderInfo>();

                var productOrders = db.Products.Join(
                    db.ProductOrders,
                    p => p.Id,
                    po => po.ProductId,
                    (p, po) => new
                    {
                        OrderId = po.OrderId,
                        ProductName = p.ProductName
                    });

                foreach(var item in orders)
                {
                    var result = productOrders.Where(l => l.OrderId == item.Id).Select(l => l.ProductName).ToList();
                    OrderInfo orderInfo = new OrderInfo() { OrderId = item.Id };
                    orderInfo.ProductNames.AddRange(result);

                    userInfoList.Add(orderInfo);
                }

                return View(userInfoList);
            }
        }


        //public ActionResult PurchasingInfo()
        //{
        //    string userId = User.Identity.GetUserId();

        //    using (ApplicationContext db = new ApplicationContext())
        //    {
        //        var productOrders = db.Products.Join(
        //            db.ProductOrders,
        //            p => p.Id,
        //            po => po.ProductId,
        //            (p, po) => new
        //            {
        //                OrderId = po.OrderId,
        //                ProductName = p.ProductName,
        //                Spent = 
        //            });

        //        return View();
        //    }
        //}
    }
}
