﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskTwo.Models
{
    public class Order
    {
        public int Id {get; set; }
        

        public DateTime DatePurchase { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public Order()
        {
            ProductOrders = new List<ProductOrder>();
        }

    }
}