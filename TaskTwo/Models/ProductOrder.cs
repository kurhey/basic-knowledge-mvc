﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskTwo.Models
{
    public class ProductOrder
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }


        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }

        public int numProducts { get; set; }


    }
}