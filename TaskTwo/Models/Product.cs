﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace TaskTwo.Models
{
    [Serializable]
    public class Product
    {
        [XmlIgnore]
        public int Id {get;set;}
        [Display(Name ="Name")]
        [Required(ErrorMessage = "Product name is required")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Product price is required")]
        public int Price { get; set; }
        [Required(ErrorMessage = "Product describtion is required")]
        public string Describe { get; set; }

        [XmlIgnore]
        public DateTime DateUpd { get; set; }

        [XmlIgnore]
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public Product()
        {
            ProductOrders = new List<ProductOrder>();
            
        }
    }
}