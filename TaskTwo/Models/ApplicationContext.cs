﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;


namespace TaskTwo.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public ApplicationContext() 
            : base("DBConnection", throwIfV1Schema: false) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Primary keys
            modelBuilder.Entity<User>().HasKey(q => q.Id);
            modelBuilder.Entity<Product>().HasKey(q => q.Id);
            modelBuilder.Entity<Order>().HasKey(q => q.Id);
            modelBuilder.Entity<ProductOrder>().HasKey(q =>
                new
                {
                    q.OrderId,
                    q.ProductId
                });

            // Relationships
            modelBuilder.Entity<ProductOrder>()
                .HasRequired(t => t.Order)
                .WithMany(t => t.ProductOrders)
                .HasForeignKey(t => t.OrderId);

            modelBuilder.Entity<ProductOrder>()
                .HasRequired(t => t.Product)
                .WithMany(t => t.ProductOrders)
                .HasForeignKey(t => t.ProductId);

        }
    }

}