﻿using System.Net;
using System.Net.Mail;

namespace TaskTwo.Data
{
    public class MailUtils
    {
        private SmtpClient smtp;

        private MailAddress from;
        private MailAddress to;
        private const string fromPassword = "fromPassword";
        private const string subject = "Congirm registration";
        private const string body = "<h2>Hello! please <a href='http://localhost:52378/Account/ConfirmAccount'>confirm</a> your account </h2>";


        public MailUtils(string userEmail)
        {
            from = new MailAddress("vetrov098@gmail.com", "Registration");
            to = new MailAddress($"{userEmail}", "To Name");

            smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(from.Address, fromPassword)
            };
        }
    }
}