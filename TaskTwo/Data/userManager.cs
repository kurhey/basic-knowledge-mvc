﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskTwo.Models;
using Microsoft.Owin.Security.DataProtection;

namespace TaskTwo.App_Start
{
    public class userManager : UserManager<User>
    {
        public userManager(IUserStore<User> store) : base(store)
        {
        }

        public static userManager Create(IdentityFactoryOptions<userManager> options, IOwinContext context)
        {
            ApplicationContext db = context.Get<ApplicationContext>();
            userManager manager = new userManager(new UserStore<User>(db));
            var provider = new DpapiDataProtectionProvider("TaskTwo");

            manager.UserTokenProvider = new DataProtectorTokenProvider<User>(
                provider.Create("TokenName"));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            manager.EmailService = new EmailService();

            return manager;
        }
    }
}